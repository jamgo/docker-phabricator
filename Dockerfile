FROM alpine:3.7
MAINTAINER Martin Espinach <martin.espinach@jamgo.coop>

ENV export BUILD_DEPS="\
      autoconf \
      php7-pear \
      php7-dev \
      alpine-sdk"

FROM alpine:3.7
MAINTAINER Martin Espinach <martin.espinach@jamgo.coop>

ENV OPENLDAP_VERSION=2.4.46

RUN \
	mkdir -vp \
		/data \
		/repo && \
	export BUILD_DEPS=" \
		autoconf \
		php7-pear \
		php7-dev \
		alpine-sdk" && \
	apk --update add \
		php7 \
		php7-dom \
		php7-ctype \
		php7-curl \
		php7-fileinfo \
		php7-fpm \
		php7-gd \
		php7-iconv \
		php7-intl \
		php7-json \
		php7-ldap \
		php7-mbstring \
		php7-mcrypt \
		php7-mysqli \
		php7-mysqlnd \
		php7-opcache \
		php7-openssl \
		php7-pcntl \
		php7-pdo \
		php7-pdo_mysql \
		php7-posix \
		php7-session \
		php7-sockets \
		php7-tidy \
		php7-xml \
		git \
		py-pygments \
		nginx \
		supervisor \
		gettext \
        ${BUILD_DEPS} && \
	printf "\n" | pecl install apcu apcu_bc-beta && \
	echo "extension=apcu.so" > /etc/php7/conf.d/apcu.ini && \
	echo "extension=apc.so"  > /etc/php7/conf.d/z_apc.ini && \
	cp /usr/bin/envsubst /usr/local/bin/envsubst && \
	apk del --purge \
		${BUILD_DEPS}

RUN \
	git clone -b stable --single-branch https://github.com/phacility/libphutil.git /srv/libphutil && \
	git clone -b stable --single-branch https://github.com/phacility/arcanist.git /srv/arcanist && \
	git clone -b stable --single-branch https://github.com/phacility/phabricator.git /srv/phabricator

COPY php.ini /etc/php7/php.ini
COPY php-fpm.conf /etc/php7/php-fpm.conf
COPY nginx.conf.template /srv/
COPY local.json.template /srv/
COPY supervisord.conf /srv/
COPY entrypoint.sh /srv/
COPY env-secrets-expand.sh /srv/

RUN set -x && \
	chmod -v +x /srv/entrypoint.sh && \
	chmod -v +x /srv/env-secrets-expand.sh

ENV PATH "/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

VOLUME ["/data", "/repo"]

ENTRYPOINT ["/srv/entrypoint.sh"]

#CMD ["supervisord", "-c", "/srv/supervisord.conf"]
