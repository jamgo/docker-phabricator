#!/bin/sh -e

. /srv/env-secrets-expand.sh

export MYSQL_ROOT_PASSWORD="${MYSQL_ROOT_PASSWORD:-password}"

if [ ! -e /etc/nginx/nginx.conf && -e /srv/nginx.conf.template ]
	cat /srv/nginx.conf.template | envsubst '${PHABRICATOR_HOST}' > /etc/nginx/nginx.conf
	rm /srv/nginx.conf.template
fi

if [ ! -e /srv/phabricator/conf/local/local.json && -e /srv/local.json.template ]
	cat /srv/local.json.template | envsubst > /srv/phabricator/conf/local/local.json
	rm /srv/local.json.template
fi

/srv/phabricator/bin/storage upgrade --force

supervisord -c /srv/supervisord.conf